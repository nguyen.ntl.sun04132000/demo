import { render, screen } from '@testing-library/react';
import App, {add} from './App';


test('renders learn react link', () => {
  render(<App />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});

test("test add func", ()=>{
  expect(add(1, 2)).toBe(3);
})
